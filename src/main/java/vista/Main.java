package vista;

import exceptions.SchoolException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Student;
import persistence.SchoolDAO;

/**
 *
 * @author Maria del Mar
 */
public class Main {

    private static SchoolDAO miDao;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        miDao = new SchoolDAO();
        try {
            System.out.println("Estableciendo conexión con la base de datos...");
            miDao.conectar();
            System.out.println("Conectado.");
            System.out.println("Insertando alumno");
            Student s = new Student(11, "Alumno", "Daw1m", 20, "java");
            try {
                miDao.insertStudent(s);
                System.out.println("Alumno registrado");
            } catch (SchoolException ex) {
                System.out.println(ex.getMessage());
            }
            s = new Student(12, "Otro", "alumno daw1m", 18, "mujer");
            try {
                miDao.insertStudent(s);
                System.out.println("Alumno registrado");
            } catch (SchoolException ex) {
                System.out.println(ex.getMessage());
            }
            System.out.println("Datos de los alumnos:");
            ArrayList<Student> students = miDao.selectAllStudentLazy();
            for (Student student : students) {
                System.out.println(student);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR con la BBDD: " + ex.getMessage());
        } finally {
            System.out.println("Desconectando....");
            try {
                miDao.desconectar();
            } catch (SQLException ex) {
                System.out.println("Error al desconectar " + ex.getMessage());
            }
        }
        System.out.println("Hasta luego!");
    }

}
